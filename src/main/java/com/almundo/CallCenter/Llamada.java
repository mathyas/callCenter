package com.almundo.CallCenter;


public class Llamada extends Thread {

    Empleado empleado;
    boolean estabaEnEspera = false;
    
    public void run() {
        try {
            System.out.println("Atediendo llamanda (enEspera: " + estabaEnEspera + ") - " + empleado.dameTipoDeEmpleado());
            switch (empleado.dameTipoDeEmpleado()) {
                case Operador:
                    Thread.sleep(10000);
                    break;
                case Director:
                    Thread.sleep(1000);
                    break;
                case Supervisor:
                    Thread.sleep(5000);
                    break;
            }
            System.out.println("Termino llamanda..." + empleado.dameTipoDeEmpleado());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            empleado.terminoLlamada();
        }
    }

    public void atender(Empleado empleado) {
        this.empleado = empleado;
    }

    public Llamada ponerEnEspera() {
        this.estabaEnEspera = true;
        return this;
    }

}
