package com.almundo.CallCenter;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class CoordinadorDeEmpleados {

    private Set<Empleado> operadores = new HashSet<Empleado>();
    private Set<Empleado> supervisores = new HashSet<Empleado>();
    private Set<Empleado> directores = new HashSet<Empleado>();
    
    public CoordinadorDeEmpleados() {
        operadores = new HashSet<Empleado>();
        supervisores = new HashSet<Empleado>();
        directores = new HashSet<Empleado>();
    }
    
    public Empleado dameEmpleadoLibre() {
        Iterator<Empleado> iterator = operadores.iterator();
        while (iterator.hasNext()) {
            Empleado next = iterator.next();
            if (next.dameLlamadaEnCurso() == null)
                return next;
        }
        iterator = supervisores.iterator();
        while (iterator.hasNext()) {
            Empleado next = iterator.next();
            if (next.dameLlamadaEnCurso() == null)
                return next;
        }
        iterator = directores.iterator();
        while (iterator.hasNext()) {
            Empleado next = iterator.next();
            if (next.dameLlamadaEnCurso() == null)
                return next;
        }
        return null;
    }

    public void registrarEmpleado(Empleado empleado) {
        switch (empleado.dameTipoDeEmpleado()) {
            case Operador:
                operadores.add(empleado);
                break;
            case Director:
                directores.add(empleado);
                break;
            case Supervisor:
                supervisores.add(empleado);
                break;
        }
    }
}
