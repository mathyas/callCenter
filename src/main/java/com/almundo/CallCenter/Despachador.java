package com.almundo.CallCenter;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class Despachador {
    
    private CoordinadorDeEmpleados coordinadorDeEmpleados;
    private Queue<Llamada> colaDeLlamadas = new ArrayBlockingQueue<Llamada>(200);
    
    private static final Despachador INSTANCE = new Despachador();

    private Despachador() {
    }
    
    public static Despachador getInstance() {
        return INSTANCE;
    }
    
    public void asignarCoordinador(CoordinadorDeEmpleados coordinador) {
        this.coordinadorDeEmpleados = coordinador;
    }
    
    public void despachar(Llamada llamada) {
        Empleado empleado = coordinadorDeEmpleados.dameEmpleadoLibre();
        if (empleado == null) 
            colaDeLlamadas.add(llamada.ponerEnEspera());
        else
            empleado.asignarLlamada(llamada);
    }
    
    public void terminoLlamada(Empleado empleadoLibre) {
        if (!colaDeLlamadas.isEmpty()) {
            Llamada llamada = colaDeLlamadas.poll();
            empleadoLibre.asignarLlamada(llamada);
        }
    }
}
