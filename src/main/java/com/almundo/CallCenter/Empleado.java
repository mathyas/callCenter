package com.almundo.CallCenter;

public class Empleado {
    private int id;
    private TipoEmpleado tipo;
    private Llamada atendiendoLlamada;
    
    public Empleado(int id, TipoEmpleado tipo) {
        this.id = id;
        this.tipo = tipo;
    }
    
    public void asignarLlamada(Llamada llamada) {
        atendiendoLlamada = llamada;
        llamada.atender(this);
        llamada.start();
    }

    public void terminoLlamada() {
        atendiendoLlamada = null;
        Despachador.getInstance().terminoLlamada(this);
    }

    public Object dameLlamadaEnCurso() {
        return atendiendoLlamada;
    }

    public TipoEmpleado dameTipoDeEmpleado() {
        return tipo;
    }
}
