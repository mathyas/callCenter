package com.almundo.CallCenter;

/**
 * Hello world!
 *
 */
public class CallCentearApp 
{
    
    private CoordinadorDeEmpleados coordinador;

    
    public CallCentearApp() {
        coordinador = new CoordinadorDeEmpleados();
        
        coordinador.registrarEmpleado(new Empleado(1, TipoEmpleado.Operador));
        coordinador.registrarEmpleado(new Empleado(1, TipoEmpleado.Operador));
        coordinador.registrarEmpleado(new Empleado(1, TipoEmpleado.Operador));
        coordinador.registrarEmpleado(new Empleado(1, TipoEmpleado.Operador));
        coordinador.registrarEmpleado(new Empleado(1, TipoEmpleado.Operador));
        coordinador.registrarEmpleado(new Empleado(1, TipoEmpleado.Operador));
        coordinador.registrarEmpleado(new Empleado(1, TipoEmpleado.Operador));
        coordinador.registrarEmpleado(new Empleado(2, TipoEmpleado.Supervisor));
        coordinador.registrarEmpleado(new Empleado(2, TipoEmpleado.Supervisor));
        coordinador.registrarEmpleado(new Empleado(3, TipoEmpleado.Director));
        
        Despachador.getInstance().asignarCoordinador(coordinador);
    }
    
    public void atenderLlamanda(Llamada llamada) {
        Despachador.getInstance().despachar(llamada);
    }
    
    public static void main( String[] args )
    {
        CallCentearApp app = new CallCentearApp();

        //ocupamos a los 10 empleados
        app.atenderLlamanda(new Llamada());
        app.atenderLlamanda(new Llamada());
        app.atenderLlamanda(new Llamada());
        app.atenderLlamanda(new Llamada());
        app.atenderLlamanda(new Llamada());
        app.atenderLlamanda(new Llamada());
        app.atenderLlamanda(new Llamada());
        app.atenderLlamanda(new Llamada());
        app.atenderLlamanda(new Llamada());
        app.atenderLlamanda(new Llamada());

        //las pone en espera
        app.atenderLlamanda(new Llamada());
        app.atenderLlamanda(new Llamada());
    }
}
